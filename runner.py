#!/usr/bin/env python
import os
import sys

import unittest
from library import tests

command = "search_book" 
if command == "search_book" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest))	
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
'''	 
if command == "search_author" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_author_redirect_if_not_logged_in))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_author_logged_in_correct_get_request))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_author_logged_in_correct_post_request))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_author_logged_in_incorrect_post_request))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
	
if command == "search_date" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_date_redirect_if_not_logged_in))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_date_logged_in_correct_get_request))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_date_logged_in_correct_post_request))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_search_date_logged_in_incorrect_post_request))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
	
if command == "registration" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_registration))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
	
if command == "comment" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_comment_redirect_if_not_logged_in))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_comment_logged_in_correct_get_request))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_comment_logged_in_correct_post_request))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
	
if command == "book_information" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_book_information_redirect_if_not_logged_in))
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_book_information_logged_in_correct_get_request))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
	
if command == "main" :
    TSuite = unittest.TestSuite()
    TSuite.addTest(unittest.makeSuite(tests.LibraryTest.test_main_correct_get_request))
    print("count of tests: " + str(TSuite.countTestCases()) + "\n")
'''
runner = unittest.TextTestRunner(verbosity=2)
runner.run(TSuite)
