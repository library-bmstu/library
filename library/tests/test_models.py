from django.test import TestCase

from library.models import Author, Book

class LibraryModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        print("setUpTestData: Run once to set up non-modified data for all class methods.")
        Author.objects.create(id = 1, name = 'Aleksandr', surname = 'Pushkin', date = '2018-05-01')
        author_ = Author.objects.get(id = 1)
        Book.objects.create(id = 1, title = 'kapytanskaya dochka', date = '2018-05-05', author = author_, info = None)

# model Author

    def test_lable_name(self):
        print("Method: test_lable_name.")
        author = Author.objects.get(id = 1)
        field_lable = author._meta.get_field('name').verbose_name
        self.assertEqual(field_lable,'Имя автора')

    def test_lable_surname(self):
        print("Method: test_lable_surname.")
        author = Author.objects.get(id = 1)
        field_lable = author._meta.get_field('surname').verbose_name
        self.assertEqual(field_lable,'Фамилия автора')

    def test_lable_date(self):
        print("Method: test_lable_date.")
        author = Author.objects.get(id = 1)
        field_lable = author._meta.get_field('date').verbose_name
        self.assertEqual(field_lable,'Дата рождения автора')

    def test_object_name(self):
        print("Method: test_object_name.")
        author = Author.objects.get(id = 1)
        expected_object_surname = '%s' % (author.surname)
        self.assertEqual(expected_object_surname, str(author))

# model Book

    #def test_lable_title(self):
        #print("Method: test_lable_title.")
        #book = Book.objects.get(id = 1)
        #field_lable = book._meta.get_field('title').verbose_name
        #self.assertEqual(field_lable,'Название книги')

    #def test_lable_date2(self):
        #print("Method: test_lable_date.")
        #book = Book.objects.get(id = 1)
        #field_lable = book._meta.get_field('date').verbose_name
        #self.assertEqual(field_lable,'Дата издания книги в формате гг-мм-дд')

    #def test_lable_info(self):
        #print("Method: test_lable_info.")
        #book = Book.objects.get(id = 1)
        #field_lable = book._meta.get_field('info').verbose_name
        #self.assertEqual(field_lable,'Расскажите нам свое мнение о книге')

    #def test_object_name2(self):
        #print("Method: test_object_name2.")
        #book = Book.objects.get(id = 1)
        #expected_object_title = '%s' % (book.title)
        #self.assertEqual(expected_object_title, str(book))

    