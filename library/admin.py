from django.contrib import admin
from .models import Book, Author
##
# \file
#
# файл для супер-пользователя (гененрируется автоматически)
admin.site.register(Book)
admin.site.register(Author)
