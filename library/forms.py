from django import forms
from .models import Book, Author
from django.contrib.auth.models import User

##
# \file
#
# файл с описанием форм

## Класс NameForm
# 
# описывает поле ввода для поиска книги по фамилии автора

class NameForm(forms.ModelForm):
	class Meta:
		model = Author
		fields = ('surname',)
		
	
	
## Класс TitleForm
# 
# описывает поле ввода для поиска книги по названию


class TitleForm(forms.ModelForm):
	class Meta:
		model = Book
		fields = ('title',)
		
## Класс DateForm
# 
# описывает поле ввода для поиска книги по дате издания
		
class DateForm(forms.ModelForm):
	class Meta:
		model = Book
		fields = ('date',)

## Класс CommentForm
# 
# описывает поле ввода для добавления отзыва
		
class CommentForm(forms.ModelForm):
	class Meta:
		model = Book
		fields = ('info',)
		
## Класс UserForm
# 
# описывает поля ввода для регистрации

		
class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('username', 'password', 'first_name', 'last_name', 'email')

